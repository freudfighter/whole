using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D Character;
        private bool Jump;


        private void Awake()
        {
            Character = GetComponent<PlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            Character.Move(h, crouch, Jump);
            Jump = false;
        }
    }
}
