﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthSystem : MonoBehaviour {
	//public Sprite [] HeartSprites;
	public Sprite img0;
	public Sprite img1;
	public Sprite img2;
	public Sprite img3;

	public Image HeartUI;

	private cont player;

	void Start(){
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<cont>();
	}
	void Update (){

		//HeartUI.sprite = HeartSprites [player.curHealth];

		if (player.curHealth == 3) {
			HeartUI.sprite = img3;
		}
		if (player.curHealth == 2) {
			HeartUI.sprite = img2;
		}
		if (player.curHealth == 1) {
			HeartUI.sprite = img1;
		}
		if (player.curHealth == 0) {
			HeartUI.sprite = img0;
		}
	}
}
