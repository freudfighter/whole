﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {
	private cont character;
	// Use this for initialization
	void Start () {
		character = gameObject.GetComponentInParent<cont> ();
	}
	
	// Update is called once per frame
	void OnTriggerEnter2d () {
		character.Grounded = true;
	}
	void OnTriggerStay2d () {
		character.Grounded = true;
	}
	void OnTriggerExit2d () {
		character.Grounded = false;
	}
}
