﻿//using System;
using UnityEngine;
using UnityEngine.SceneManagement; // add to load scenes
using System.Collections; //add to use IEnumerator

[RequireComponent(typeof(AudioSource))]
	public class cont : MonoBehaviour
	{
	public float maxSpeed=500f;  
	 public float Speed = 100f;                    // The fastest the player can travel in the x axis.
	 public float JumpForce = 5.50f;                  // Amount of force added when the player jumps.
	// private bool AirControl = true;                 // Whether or not a player can steer while jumping;
    // private LayerMask WhatIsGround;                  // A mask determining what is ground to the character

		public bool Grounded;            // Whether or not the player is grounded.
		private Animator Anim;            // Reference to the player's animator component.
		private Rigidbody2D Rigidbody2D;
		//private bool FacingRight = true;  // For determining which way the player is currently facing.

		public AudioClip jumping; // jumping audio file
		AudioSource audio; //audio source setup

		private GameMaster gm; //GameMaster script
		public AudioClip coinCollect; // coin collect audio file

		//Stats
		public int curHealth; //current health integer
		public int maxhealth = 3; // max health integer = full health

		public AudioClip damageSfx;
		public Transform firePoint;
		public GameObject cigar;
	public bool Attack;
	public bool hurt;
	private EnemyScript Enemy;
	public float duration;
	public Vector3 respawnPoint;




	 void Awake()
		{
			// Setting up references.
		    Anim = gameObject.GetComponent<Animator>();
			Rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
		//Enemy = GameObject.FindGameObjectWithTag ("Enemy").GetComponent<EnemyScript> ();
		}

		void Start() {

			audio = GetComponent<AudioSource> (); // access audio source component

			gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>(); // access the GameMaster script from the GameMaster gameobject

			curHealth = maxhealth; // at start of game, health is always full

		}

		void Update () {
		
		Anim.SetBool ("isGrounded", Grounded);
		Anim.SetBool ("Attack", false);
		Anim.SetBool ("isDamaged", hurt);
		Anim.SetFloat ("Speed", Mathf.Abs (Rigidbody2D.velocity.x));

		float h = Input.GetAxis("Horizontal");
		Rigidbody2D.AddForce((Vector2.right* Speed)*h);

//		if (Input.GetKeyDown (KeyCode.Space) ) {
//			Rigidbody2D.AddForce(Vector2.up * JumpForce*100f);
//		}
		if (Input.GetKeyDown (KeyCode.Space)&& Grounded) {
			Anim.SetBool ("isGrounded", false);
			Rigidbody2D.AddForce(Vector2.up * JumpForce*95f);
			audio.PlayOneShot (jumping, 1.0f); 
			}

			if (Input.GetAxis("Horizontal")<-.1f) {
			transform.localRotation = Quaternion.Euler(0, 180, 0);
			//			if(transform.rotation != Quaternion.Euler(0, 180, 0)){
//				transform.rotation = Quaternion.Euler(0, 180, 0);
//		}
			//transform.localScale = new Vector3(-1f,1f,1f);
		}

		if (Input.GetAxis("Horizontal")>.1f) {
			transform.localRotation = Quaternion.Euler(0, 0, 0);
			//			if (transform.rotation != Quaternion.Euler (0, 0, 0)) {
//				transform.rotation = Quaternion.Euler (0, 0, 0);
//			}
			//transform.localScale = new Vector3(1f,1f,1f);
		}
		if (Input.GetKeyDown (KeyCode.C)&& Attack==false) {
			Anim.SetBool ("Attack", true);
			Instantiate(cigar, firePoint.position, firePoint.rotation);


		}
			if (curHealth > maxhealth) {

				curHealth = maxhealth; //always sets health to maximum at start of game

			}

			if (curHealth <= 0) {

				StartCoroutine ("DelayedRestart"); //if health is less than or equal to 0, restart the scene with a delay (IEnumerator)

			}


		}

	void FixedUpdate(){
		Vector3 easeVelocity = Rigidbody2D.velocity;
		easeVelocity.y = Rigidbody2D.velocity.y;
		easeVelocity.z = 10f;
		easeVelocity.x *= 0.75f;
	
		if (Grounded) {

			Rigidbody2D.velocity = easeVelocity;
		}
		if (Rigidbody2D.velocity.x > maxSpeed) {
		
			Rigidbody2D.velocity = new Vector2 (maxSpeed, 100f);
		}
		if (Rigidbody2D.velocity.x < -maxSpeed) {

			Rigidbody2D.velocity = new Vector2 (-maxSpeed, 100f);

		}
	}
		/*
		 * public void Move(float move, bool jump)
		{
		
			//only control the player if grounded or airControl is turned on
			if (Grounded || AirControl)
			{

				// The Speed animator parameter is set to the absolute value of the horizontal input.
				Anim.SetFloat("Speed", Mathf.Abs(move));

				// Move the character
				Rigidbody2D.velocity = new Vector2(move*maxSpeed, Rigidbody2D.velocity.y);

				// If the input is moving the player right and the player is facing left...
				if (move > 0 && !FacingRight)
				{
					// ... flip the player.
					Flip();
				}
				// Otherwise if the input is moving the player left and the player is facing right...
				else if (move < 0 && FacingRight)
				{
					// ... flip the player.
					Flip();
				}
			}
			// If the player should jump...
			if (Grounded && jump && Anim.GetBool("Ground"))
			{
				// Add a vertical force to the player.
				Grounded = false;
				Anim.SetBool("Ground", false);
				Rigidbody2D.AddForce(new Vector2(0f, JumpForce));
				audio.PlayOneShot (jumping, 1.0f); //play sfx
			}
		}


		private void Flip()
		{
			// Switch the way the player is labelled as facing.
			FacingRight = !FacingRight;

			// Multiply the player's x local scale by -1.
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
*/

		void OnTriggerEnter2D(Collider2D col) {

			if (col.CompareTag ("Coin")) { // if player collides with a gameobject of tag "coin" then...

				Destroy (col.gameObject); //delete the gameobject
				audio.PlayOneShot (coinCollect, 1.0f); // play coin collect sfx
				gm.points += 1; //add a point to coin collection

			}
//		if (col.CompareTag ("Enemy")) { // if player collides with a gameobject of tag "coin" then...
//
//			audio.PlayOneShot (damageSfx, 1.0f); // play coin collect sfx
//			Anim.Play ("Damage");
//		}
//		if (col.CompareTag ("Fall Detector")) {
//			transform.position = respawnPoint;
//		}

		}
	
		void Death () {

		UnityEngine.SceneManagement.SceneManager.LoadScene ("freud1"); //load scene 1


		}

		IEnumerator DelayedRestart () {
		Anim.SetBool ("isDead", true);
		GetComponent<Animation>().Play("freud_die");
			yield return new WaitForSeconds (1); //delay by 1 second
			Death (); //execute Die method

		}

		public void Damage (int dmg) {

			audio.PlayOneShot(damageSfx, 1.0f); //play damage audio file
			GetComponent<Animation>().Play("freud_damage"); //play damage animation
			curHealth -= dmg; // take -1 damage

		}

		public IEnumerator Knockback(float knockDur, float knockbackPwr, Vector3 knockbackDir) { //knockDur is how long we will add force for knockback, knockbackPwr is the amount of force, knockbackDir is the direction we are sending the player

			float timer = 0; //counting the time elapsed of the method

			while (knockDur > timer) { //while the length of the added force is greater than 0...

				timer += Time.deltaTime; //add time to the timer, counts in seconds

				Rigidbody2D.AddForce (new Vector3 (knockbackDir.x * -125, knockbackDir.y * knockbackPwr, transform.position.z)); //add force to move character's position to the left on the X axis and slightly on Y, no change on Z
			}

			yield return 0; //when while condition is not met, the coroutine stops


		}


	}


