﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

	public LayerMask enemyMask;
	public float speed = 10f;
	Rigidbody2D myBody;
	Transform myPosition;
	float myHeight, myWidth;

	// Use this for initialization
	void Start () {
		SpriteRenderer mySprite = this.GetComponent<SpriteRenderer> ();

		myPosition = this.transform;
		myBody = this.GetComponent<Rigidbody2D> ();
		myHeight = mySprite.bounds.extents.y;
		myWidth = mySprite.bounds.extents.x;

	}


	void FixedUpdate(){
	
		Vector2 lineCastPos = myPosition.position.toVector2() - myPosition.right.toVector2() * myWidth + Vector2.up * -3.5f;
		Debug.DrawLine (lineCastPos, lineCastPos + Vector2.down);
		bool isGrounded = Physics2D.Linecast (lineCastPos, lineCastPos + Vector2.down, enemyMask);
		Debug.DrawLine (lineCastPos, lineCastPos - myPosition.right.toVector2()*.5f);
		bool isBlocked = Physics2D.Linecast (lineCastPos, myPosition.right.toVector2()*.5f, enemyMask);


		if (!isGrounded || !isBlocked) {
			Vector3 currRot = myPosition.eulerAngles;
			currRot.y += 180;
			myPosition.eulerAngles = currRot;
		}
		Vector2 myVelocity = myBody.velocity;
		myVelocity.x = myPosition.right.x * -speed;
		myBody.velocity = myVelocity;
	}

}
