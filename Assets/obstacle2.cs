﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacle2 : MonoBehaviour {
	private cont player;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<cont>();
	}

	// Update is called once per frame
	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			player.hurt =true;
			player.Damage (1);
			StartCoroutine (player.Knockback (0.02f, 300, player.transform.position));
		} else{
			player.hurt=false;
		}
	}
		void OnCollisionExit2D (Collision2D coll){
			if (coll.gameObject.tag == "Player") {
				player.hurt =false;
			}

		}

}
