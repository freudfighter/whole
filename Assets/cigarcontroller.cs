﻿using System.Collections;
using UnityEngine;

public class cigarcontroller : MonoBehaviour {
	public float speed;
	public cont player;
	public int enemyDamage=0;
	// Use this for initialization
	void Start () {
		player = FindObjectOfType<cont>();
		if(player.transform.localScale.x < 0){
			speed = -speed;
		}
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
	}
//		void OnCollisionExit2D (Collision2D coll){
//			if(coll.gameObject.tag == "Enemy"){
//				Destroy (coll.gameObject);
//			}
//
//			Destroy (gameObject);
//		}
	void OnTriggerEnter2D (Collider2D col) {
		if (col.CompareTag("Stomper")){
			enemyDamage = enemyDamage + 1;
			StartCoroutine("DeathDelay");
		}
	}

	IEnumerator DeathDelay(){
		//audio.PlayOneShot (enemyDeath, 1.0f);
		yield return new WaitForSeconds (.5f);
		Destroy(transform.parent.gameObject);
		Destroy (gameObject);
		Debug.Log("Murderrrr");
	}
}
