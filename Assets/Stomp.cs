﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Stomp : MonoBehaviour {
	public AudioClip enemyDeath;
	public float delay;
	AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) {
		if (col.CompareTag("Stomper")){
			StartCoroutine("DeathDelay");
		}
	}

	IEnumerator DeathDelay(){
		audio.PlayOneShot (enemyDeath, 1.0f);
		yield return new WaitForSeconds (delay);
		Destroy(transform.parent.gameObject);
		Debug.Log("Murderrrr");
	}

}
